package antochi.com.dotviewtest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Vibrator;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class DotMatrixView
        extends View
        implements GestureDetector.OnGestureListener
{

    private static final int DOTVIEW_WIDTH = 27;
    private static final int DOTVIEW_HEIGHT = 48;

    private DotActivity mainContext = null;
    private Vibrator vibrator = null;
    private DisplayMetrics display = null;
    private GestureDetectorCompat gestureDetector = null;
    private FlappyGame game = null;

    public DotMatrixView(Context context) {
        super(context);
        InitData();
    }

    public DotMatrixView(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitData();
    }

    public DotMatrixView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        InitData();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        setMeasuredDimension(width,height);

    }

    @Override
    protected void onDraw(Canvas canvas) {

        int pixel_scale = display.widthPixels / DOTVIEW_WIDTH;
        game.DrawFrame(canvas, pixel_scale);

    }

    private void InitData()
    {
        mainContext = DotActivity.GetCurrentActivity();
        vibrator = (Vibrator)(mainContext.getSystemService(Context.VIBRATOR_SERVICE));
        gestureDetector = new GestureDetectorCompat(mainContext, this);
        display = new DisplayMetrics();

        Display d = ((WindowManager)mainContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        d.getMetrics(display);

        game = new FlappyGame();
        game.Init();

    }

    private void PerformGameReaction()
    {
        game.Action();
        vibrator.vibrate(100);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return true;
    }

    ///////////////////////////////////////////
    //          Gestures methods
    ///////////////////////////////////////////

    @Override
    public boolean onDown(MotionEvent event) {

        int x = (int)((event.getX() / display.widthPixels) * DOTVIEW_WIDTH);
        int y = (int)((event.getY() / display.heightPixels) * DOTVIEW_HEIGHT);
        Log.i(DotActivity.LOG, x + ":" + y);

        if(game.isPointInGameObject(new Point(x,y)))
        {
            if (!game.isGameStarted()) game.StartGame();
            PerformGameReaction();
        }

        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if(game.isGameLost()) {
            game.StopGame();
            game.Init();
            this.invalidate();
        }
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    /////////////////////////////////////
    // Nested Game Class
    ////////////////////////////////////

    private class FlappyGame{

        private Rect gameObject = null;
        private int objectSize = 8;
        private int userScore = 0;
        private boolean isGameLost = false;
        private boolean gameStarted = false;
        private boolean isUp = false;
        private Timer gameTimer = null;

        public void Init()
        {
            userScore = 0;
            isGameLost = false;

            gameObject = new Rect(
                    DOTVIEW_WIDTH/2 - objectSize /2, DOTVIEW_HEIGHT/2 - objectSize /2,
                    DOTVIEW_WIDTH/2 + objectSize /2, DOTVIEW_HEIGHT/2 + objectSize /2
            );
        }

        public void DrawFrame(Canvas canvas, int scale)
        {
            canvas.scale(scale, scale);
            clearScreen(canvas);

            if(!isGameLost) {
                drawTexts(canvas);
                drawGameRect(canvas);
            } else
            {
                drawLostGameText(canvas);
            }
        }

        public void StopGame()
        {
            gameStarted = false;
            isGameLost = true;
            if(gameTimer!=null) gameTimer.cancel();
            gameTimer = null;
        }

        public void StartGame()
        {
            isGameLost = false;
            gameStarted = true;
            gameTimer = new Timer(true);
            gameTimer.schedule(new TimerAction(), 1, 45);
        }

        public boolean isGameStarted()
        {
            return gameStarted;
        }

        public boolean isGameLost()
        {
            isGameLost = gameObject.bottom>DOTVIEW_HEIGHT;
            return isGameLost;
        }

        public boolean isBlockMovingUp()
        {
            return isUp;
        }

        public void StopBlockMovingUp()
        {
            isUp = false;
        }

        public void ChanageBlockPosition(int points)
        {
            gameObject.top += points;
            gameObject.bottom += points;
        }

        public void Action()
        {
            MoveRectUp();
            userScore++;
        }

        public boolean isPointInGameObject(Point p)
        {
            return p.x >= gameObject.left && p.x <= gameObject.right &&
                    p.y >= gameObject.top && p.y <= gameObject.bottom;
        }

        private void clearScreen(Canvas canvas)
        {
            //setBackgroundColor(Color.WHITE);
            Paint clearPaint = new Paint();
            clearPaint.setColor(Color.rgb(0,44,86));
            canvas.drawRect(0,0,DOTVIEW_WIDTH,DOTVIEW_HEIGHT,clearPaint);
        }

        private void drawGameRect(Canvas canvas)
        {
            Paint p = new Paint();
            p.setColor(Color.RED);
            canvas.drawRect(gameObject, p);
        }

        private void drawTexts(Canvas canvas)
        {
            Paint p = new Paint();
            p.setColor(Color.WHITE);
            canvas.drawText(userScore +"", 0, DOTVIEW_HEIGHT-6, p);
        }

        private void drawLostGameText(Canvas canvas)
        {
            Paint p = new Paint();
            p.setColor(Color.CYAN);
            canvas.drawText("GAME", 0, DOTVIEW_HEIGHT / 4, p);
            canvas.drawText("OVER", 0, DOTVIEW_HEIGHT / 4 * 3, p);
        }

        private void MoveRectUp()
        {
            isUp = true;
        }


        private class TimerAction extends TimerTask
        {

            private int up_buffer = 0;

            @Override
            public void run() {

                if(FlappyGame.this.isBlockMovingUp())
                {
                    int upMotion = new Random().nextInt(4);
                    FlappyGame.this.ChanageBlockPosition(-upMotion);
                    if(++up_buffer>=5) {
                        up_buffer=0;
                        FlappyGame.this.StopBlockMovingUp();
                    }
                } else FlappyGame.this.ChanageBlockPosition(2);

                if(FlappyGame.this.isGameLost())
                    FlappyGame.this.StopGame();

                DotMatrixView.this.mainContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DotMatrixView.this.invalidate();
                    }
                });
            }

        }

    }

}
