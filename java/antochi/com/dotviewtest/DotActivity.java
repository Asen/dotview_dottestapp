package antochi.com.dotviewtest;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class DotActivity extends Activity {

    final static public String LOG = "[DotViewSimpleTestApp]";
    private static DotActivity DOT_CONTEXT = null;

    private DotViewServiceConnection mServiceConnect = new DotViewServiceConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DOT_CONTEXT = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dotview);
        int FullScreenFlag = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setFlags(FullScreenFlag, FullScreenFlag);

        WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        localLayoutParams.screenBrightness = 1.0F;

        // this is ALSO important to set: window MUST BE visible when screen is locked
        localLayoutParams.flags |= WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;

        getWindow().setAttributes(localLayoutParams);

        // Binding to "special" DotView Service
        Intent localIntent = new Intent();
        localIntent.setClassName("com.htc.dotmatrix", "com.htc.dotmatrix.GameService");
        this.bindService(localIntent, mServiceConnect, BIND_AUTO_CREATE);

    }

    public static void KillCurrentActivity()
    {
        if(DOT_CONTEXT!=null) {
            DOT_CONTEXT.unbindService(DOT_CONTEXT.GetEstablishedConnection());
            DOT_CONTEXT.finish();
            DOT_CONTEXT = null;
        }
    }

    public static DotActivity GetCurrentActivity()
    {
        return DOT_CONTEXT;
    }

    public ServiceConnection GetEstablishedConnection()
    {
        return mServiceConnect;
    }



    public class DotViewServiceConnection implements ServiceConnection {

        public Messenger mService = null;

        private Handler mInnerHandler = new Handler()
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                super.handleMessage(paramAnonymousMessage);

                Log.i(DotActivity.LOG, "mInnerMessenger, msg.what: " + paramAnonymousMessage.what);
                if (mInnerHandler.hasMessages(1))
                    mInnerHandler.removeMessages(1);
            }
        };


        private Messenger mMessenger = new Messenger(new Handler()
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                super.handleMessage(paramAnonymousMessage);

                Log.i(DotActivity.LOG, "handleMessage: " + paramAnonymousMessage.what);
                String str = "Got:" + paramAnonymousMessage.arg1;
                Bundle localBundle = paramAnonymousMessage.getData();
                if (localBundle != null)
                    str = str + localBundle.getString("status");
                Log.i(DotActivity.LOG, "text=" + str);
                //Toast.makeText(DotActivity.this, str, Toast.LENGTH_LONG).show();
            }
        });

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            Log.i(DotActivity.LOG, "Connection: +");
            this.mService = new Messenger(service);

            // Performing "MSG_DOTVIEW_CLIENT_REGISTER"
            this.RegistrationOperation();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(DotActivity.LOG, "Connection: -");
            Log.i(DotActivity.LOG, "onServiceDisconnected:" + name);
            this.mService = null;
        }

        private void RegistrationOperation()
        {
            Log.i(DotActivity.LOG, "RegistrationOperation is complete.");

            try
            {
                Message localMessage = Message.obtain(null, 1);
                localMessage.replyTo = this.mMessenger;
                Bundle localBundle = new Bundle();
                localBundle.putString("activityName", DotActivity.class.getName());
                localMessage.setData(localBundle);
                this.mService.send(localMessage);

                // Performing "MSG_DOTVIEW_CLIENT_RESUME"
                this.ResumeOperation();

                Log.i(DotActivity.LOG, "Connection: success");
                return;
            }
            catch (RemoteException localRemoteException)
            {
                Log.i(DotActivity.LOG, "Sending is failed: " + localRemoteException, localRemoteException);
            }
        }

        private boolean ResumeOperation()
        {
            Log.i(DotActivity.LOG, "ResumeOperation is complete.");

            if (this.mService != null)
                try
                {
                    Message localMessage = Message.obtain(null, 3);
                    Bundle localBundle = new Bundle();
                    localBundle.putString("activityName", DotActivity.class.getName());
                    localMessage.setData(localBundle);
                    this.mService.send(localMessage);
                    return true;
                }
                catch (Exception localRemoteException)
                {
                    Log.i(DotActivity.LOG, "Sending is failed: " + localRemoteException, localRemoteException);
                    return false;
                }
            if (this.mInnerHandler.hasMessages(1))
                this.mInnerHandler.removeMessages(1);
            this.mInnerHandler.sendMessageDelayed(Message.obtain(null, 1, 0, 100), 100);
            return false;
        }

    }
}
