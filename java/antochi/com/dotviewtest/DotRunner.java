package antochi.com.dotviewtest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;


public class DotRunner extends ActionBarActivity {

    // custom dotview broadcast event
    private final String DOT_EVENT = "com.htc.cover.closed";

    private IntentFilter intentFilter = new IntentFilter(DOT_EVENT);
    private BroadcastReceiver dotReceiver =  new DotReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dot_runner);

        // register custom Dot View receiver for watching cover state
        registerReceiver(dotReceiver,intentFilter);
    }

    public class DotReceiver extends BroadcastReceiver {

        private boolean isCloseCoverRegistrated = false;

        @Override
        public void onReceive(Context context, Intent intent) {

            // "state" - 'true' if closed, 'false' otherwise
            boolean isCoverClosed = intent.getBooleanExtra("state",false);
            Log.i(DotActivity.LOG, "DotView isClosed: " + isCoverClosed);

            if(isCoverClosed)
            {
                startActivity(new Intent(DotRunner.this, DotActivity.class));
                isCloseCoverRegistrated = true;
            }
            else{

                if(isCloseCoverRegistrated) {
                    DotActivity.KillCurrentActivity();
                    //DotRunner.this.finish();
                    Log.i(DotActivity.LOG, "DotActivity was killed.");
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        unregisterReceiver(dotReceiver);
        super.onBackPressed();
    }
}
